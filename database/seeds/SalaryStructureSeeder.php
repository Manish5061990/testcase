<?php

use Illuminate\Database\Seeder;
use App\Models\Users\Salary_Structure;

class SalaryStructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $Salary_Structure=[
        [
            "UserID"=>"100002",
            "basic"=>"10000.00",
            "HRA"=>"2000.00",
            "DA"=>"1500.00"
        ],
        [
            "UserID"=>"100003",
            "basic"=>"12000.00",
            "HRA"=>"3000.00",
            "DA"=>"1600.00"
        ],
        [
            "UserID"=>"100004",
            "basic"=>"15000.00",
            "HRA"=>"4000.00",
            "DA"=>"1800.00"
        ],
        [
            "UserID"=>"100005",
            "basic"=>"18000.00",
            "HRA"=>"5000.00",
            "DA"=>"1900.00"
        ],
        [
            "UserID"=>"100006",
            "basic"=>"20000.00",
            "HRA"=>"6000.00",
            "DA"=>"1900.00"
        ],
        [
            "UserID"=>"100007",
            "basic"=>"22000.00",
            "HRA"=>"7000.00",
            "DA"=>"1500.00"
        ],
        [
            "UserID"=>"100008",
            "basic"=>"24000.00",
            "HRA"=>"8000.00",
            "DA"=>"1600.00"
        ],
        [
            "UserID"=>"100009",
            "basic"=>"26000.00",
            "HRA"=>"9000.00",
            "DA"=>"1900.00"
        ],
        [
            "UserID"=>"100010",
            "basic"=>"28000.00",
            "HRA"=>"9000.00",
            "DA"=>"1200.00"
        ],
        [
            "UserID"=>"100011",
            "basic"=>"30000.00",
            "HRA"=>"7000.00",
            "DA"=>"1300.00"
        ],
        [
            "UserID"=>"100012",
            "basic"=>"32000.00",
            "HRA"=>"9000.00",
            "DA"=>"1600.00"
        ],
        [
            "UserID"=>"100013",
            "basic"=>"34000.00",
            "HRA"=>"6000.00",
            "DA"=>"1500.00"
        ],
        [
            "UserID"=>"100014",
            "basic"=>"36000.00",
            "HRA"=>"8000.00",
            "DA"=>"1900.00"
        ],
        [
            "UserID"=>"100015",
            "basic"=>"38000.00",
            "HRA"=>"7000.00",
            "DA"=>"1400.00"
        ],
        [
            "UserID"=>"100016",
            "basic"=>"40000.00",
            "HRA"=>"5000.00",
            "DA"=>"1800.00"
        ],

    ];

    public function run()
    {
        foreach($this->Salary_Structure as $Salary_Structure){
            $Salary_Structure_table= Salary_Structure::where('basic','=',$Salary_Structure['basic'])->get();
            $count= count($Salary_Structure_table);
            if($count == 0){
                $name=Salary_Structure::create([
                    'UserID'=>$Salary_Structure['UserID'],
                    'basic'=> $Salary_Structure['basic'],
                    'HRA'=>$Salary_Structure['HRA'],
                    'DA'=>$Salary_Structure['DA']
                    ]);
            }
        }


    }
}
