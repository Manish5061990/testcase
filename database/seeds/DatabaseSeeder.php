<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $tables = [
        'oauth_scopes',
        'oauth_grants',
        'oauth_grant_scopes',
        'oauth_clients',
        'oauth_client_endpoints',
        'oauth_client_scopes',
        'oauth_client_grants',
        'oauth_sessions',
        'oauth_session_scopes',
        'oauth_auth_codes',
        'oauth_auth_code_scopes',
        'oauth_access_tokens',
        'oauth_access_token_scopes',
        'oauth_refresh_tokens',
        'user_role',
        'users'
    ];


    public function run()
    {
        Model::unguard();
        if (env('APP_ENV') == 'local') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            // truncate all tables in tables list
            foreach ($this->tables as $table) {
                DB::table($table)->truncate();
            }
        }

       // These seeders run regardless of environment
        // They are not client-specific seeders
        $this->call(seeds\project\configuration\OAuthSecuritySeeder::class);
        $this->call('UsersRoleSeeder');
        $this->call('UsersTableSeeder');
        $this->call('SalaryStructureSeeder');

    }
}


