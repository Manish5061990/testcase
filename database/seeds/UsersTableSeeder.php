<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Administrator')
            ->first()
            ->UserRoleID;

        $employee_role = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Employee')
            ->first()
            ->UserRoleID;

        $faker = Faker::create();

        if (env('APP_ENV') == 'local') {
            DB::table('users')->insert(array(
                array(
                    'id'=>'100001',
                    'name'=> 'Manish ',
                    'UserRoleID' => $admin_role,
                    'email' => 'manish@demo.com',
                    'username' => 'administrator',
                    'password' => Hash::make('password'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),

                 array(
                     'id'=>'100002',
                     'name'=> 'Abhishek',
                     'UserRoleID' => $employee_role,
                     'email' => 'abhishek@demo.com',
                     'username' => 'Abhishek',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),

                array(
                    'id'=>'100003',
                    'name'=> 'Muzzu',
                    'UserRoleID' => $employee_role,
                    'email' => 'muzzu@demo.com',
                    'username' => 'Muzzu',
                    'password' => Hash::make('password'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                 array(
                     'id'=>'100004',
                     'name'=> 'Ajay',
                     'UserRoleID' => $employee_role,
                     'email' => 'ajay@demo.com',
                     'username' => 'Ajay',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                array(
                    'id'=>'100005',
                    'name'=> 'Krishnakant',
                    'UserRoleID' => $employee_role,
                    'email' => 'kk@demo.com',
                    'username' => 'Krishnakant',
                    'password' => Hash::make('password'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                  array(
                      'id'=>'100006',
                      'name'=> 'Nikhil',
                      'UserRoleID' => $employee_role,
                      'email' => 'nikhil@demo.com',
                      'username' => 'Nikhil',
                      'password' => Hash::make('password'),
                      'created_at' => Carbon::now(),
                      'updated_at' => Carbon::now()
                  ),

                 array(
                     'id'=>'100007',
                     'name'=> 'Priyanka',
                     'UserRoleID' => $employee_role,
                     'email' => 'priyanka@demo.com',
                     'username' => 'Priyanka',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                  array(
                      'id'=>'100008',
                      'name'=> 'Shweta',
                      'UserRoleID' => $employee_role,
                      'email' => 'shweta@demo.com',
                      'username' => 'Shweta',
                      'password' => Hash::make('password'),
                      'created_at' => Carbon::now(),
                      'updated_at' => Carbon::now()
                  ),
                array(
                    'id'=>'100009',
                    'name'=> 'Deepak',
                    'UserRoleID' => $employee_role,
                    'email' => 'deepak@demo.com',
                    'username' => 'Deepak',
                    'password' => Hash::make('password'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),

                 array(
                     'id'=>'100010',
                     'name'=> 'Dinesh',
                     'UserRoleID' => $employee_role,
                     'email' => 'dinesh@demo.com',
                     'username' => 'Dinesh',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                array(
                    'id'=>'100011',
                    'name'=> 'Deepika',
                    'UserRoleID' => $employee_role,
                    'email' => 'deepika@demo.com',
                    'username' => 'Deepika',
                    'password' => Hash::make('password'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                 array(
                     'id'=>'100012',
                     'name'=> 'Noman',
                     'UserRoleID' => $employee_role,
                     'email' => 'noman@demo.com',
                     'username' => 'Noman',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                 array(
                     'id'=>'100013',
                     'name'=> 'Vikas',
                     'UserRoleID' => $employee_role,
                     'email' => 'vikas@demo.com',
                     'username' => 'Vikas',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                 array(
                     'id'=>'100014',
                     'name'=> 'Nilesh',
                     'UserRoleID' => $employee_role,
                     'email' => 'nilesh@demo.com',
                     'username' => 'Nilesh',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 ),
                  array(
                      'id'=>'100015',
                      'name'=> 'Chirag',
                      'UserRoleID' => $employee_role,
                      'email' => 'chirag@demo.com',
                      'username' => 'Chirag',
                      'password' => Hash::make('password'),
                      'created_at' => Carbon::now(),
                      'updated_at' => Carbon::now()
                  ),
                 array(
                     'id'=>'100016',
                     'name'=> 'Udit',
                     'UserRoleID' => $employee_role,
                     'email' => 'udit@demo.com',
                     'username' => 'Udit',
                     'password' => Hash::make('password'),
                     'created_at' => Carbon::now(),
                     'updated_at' => Carbon::now()
                 )

            ));
        }      
    }
}