<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades;

class CreateMonthlySalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('monthly_salary')) {
            Schema::create('monthly_salary', function (Blueprint $table) {
                $table->Increments('id');
                $table->integer('UserID')->unsigned()->nullable();
                $table->decimal('basic',10,2)->nullable();
                $table->decimal('HRA',10,2)->nullable();
                $table->decimal('DA',10,2)->nullable();
                $table->string('month')->nullable();
                $table->string('year')->nullable();
                $table->timestamps();

                $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monthly_salary');
    }
}
