<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades;

class SalaryStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('salary_structure')) {
            Schema::create('salary_structure', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('UserID')->unsigned()->nullable();
                $table->decimal('basic',10,2)->nullable();
                $table->decimal('HRA',10,2)->nullable();
                $table->decimal('DA',10,2)->nullable();
                $table->timestamps();

                $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_structure');
    }
}
