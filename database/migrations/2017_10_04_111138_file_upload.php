<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('file_upload')) {
            Schema::create('file_upload', function (Blueprint $table) {
                $table->Increments('id');
                $table->boolean('is_file_uploaded')->default(0);
                $table->string('FileType')->nullable();
                $table->string('FilePath')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_upload');
    }
}
