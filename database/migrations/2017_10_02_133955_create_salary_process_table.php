<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades;

class CreateSalaryProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('salary_process')) {
            Schema::create('salary_process', function (Blueprint $table) {
                $table->Increments('id');
                $table->decimal('total',10,2)->nullable();
                $table->integer('UserID')->unsigned();
                $table->string('pay_days')->nullable();
                $table->string('month')->nullable();
                $table->string('year')->nullable();
                $table->timestamps();

                $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_process');
    }
}
