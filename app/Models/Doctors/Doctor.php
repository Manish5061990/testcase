<?php
/**
 * Created by PhpStorm.
 * User: Manish
 * Date: 26-08-2017
 * Time: 15:47
 */

namespace app\Models\Doctors;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model{

    protected $table = 'doctors';

    protected $fillable = [

        'DoctorID', 'UserID','first_name','last_name','gender','date_of _birth','specialization','address',
        'city','state','pincode','country','degree','EmailId','age','status','created_at','updated_at'
    ];

}