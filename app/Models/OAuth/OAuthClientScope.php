<?php
/**
 * Created by PhpStorm.
 * User: Manish
 * Date: 21-08-2017
 * Time: 23:26
 */

namespace App\Models\OAuth;
use Illuminate\Database\Eloquent\Model;

class OAuthClientScope extends Model
{
    protected  $table="oauth_scopes";
}