<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Salary_Structure extends Model
{
    protected $table = 'salary_structure';

    protected $fillable = [

            'id','UserID','basic','HRA','DA','created_at', 'updated_at'
    ];
}
