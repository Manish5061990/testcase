<?php

namespace App\Models\Users;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';

    protected $fillable = [
        'id',
        'UserRoleID',
        'name',
        'email',
        'username',
        'password',
        'created_at',
        'updated_at'

    ];
}
