<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Salary_Process extends Model
{
    protected $table = 'salary_process';

    protected $fillable = [

        'basics','HRA','DA',
        'id','total','UserID','pay_days','month','year','created_at', 'updated_at'
    ];
}
