<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class FileUploadModel extends Model
{
    protected $table = 'file_upload';

    protected $fillable = [

        'is_file_uploaded','FileType','FilePath','CreatedBy','created_at', 'updated_at'
    ];
}
