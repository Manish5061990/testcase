<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Userrole extends Model
{
    protected $table = 'user_role';

    protected $fillable = [

        'UserRoleID', 'UserRole','created_at','updated_at'
    ];
}
