<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Monthly_Salary extends Model
{
    protected $table = 'monthly_salary';

    protected $fillable = [

            'id','UserID','basic','HRA','DA','month','year','created_at', 'updated_at'
    ];
}
