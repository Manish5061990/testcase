<?php

namespace App\Models\Patients;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patients';

    protected $fillable = [

       'PatientID', 'UserID', 'first_name','last_name','gender','date_of _birth','address','city','state','pincode','country','EmailId','age','status','created_at','updated_at'
    ];
}
