<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Users\Salary_Structure;
use App\Models\Users\Monthly_Salary;
use App\Models\Users\Salary_Process;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
class FileUploadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FileUpload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will Run File Upload data using chunk method';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }





    public function handle()
    {
        //$contents = Storage::get('LoadEmpData.csv');
        //dd($contents);
       // $filePath = 'LoadEmpData.csv';
        //$path = public_path()."/".$filePath
        ////$sheet = file_get_contents(base_path() . '/storage/uploads/LoadEmpData.csv');
       // dd($sheet);
        //$destinationpath = storage_path() . '/uploads';
        //$content = File::get($destinationpath);
        //dd($content);


        $total = DB::table('users')
            ->where('id', '!=', 100001)
            ->select('users.*')
            ->get();

        if (!empty($total)) {


            //$customerArr = $this->csvToArray(Input::file('LoadEmpData'));
            Excel::filter('chunk')->load($path)->chunk(250, function ($customerArr) {
                foreach ($customerArr as $memu) {
                    $result[] = $memu['user_id'];
                }
                $length = count($result);
                for ($k = 0; $k < $length; $k++) {
                    $rse = DB::table('users')
                        ->where('id', '=', $result[$k])
                        ->select('users.*')
                        ->get();

                    if (!empty($rse)) {

                        $doc = DB::table('employee_db.salary_structure')
                            ->where('UserID', '=', $rse[0]->id)
                            ->select('basic', 'HRA', 'DA')
                            ->get();

                        if (!empty($doc)) {
                            $basic = $doc[0]->basic;
                            $hra = $doc[0]->HRA;
                            $da = $doc[0]->DA;
                        } else {
                            $basic = null;
                            $hra = null;
                            $da = null;
                        }
                        if ($customerArr[$k]['pay_days'] <= 31) {
                            $pay_days = $customerArr[$k]['pay_days'];
                            $month = $customerArr[$k]['month'];
                            $year = $customerArr[$k]['year'];

                            //calculate salary basis on below formulaa
                            $calcBasics = $basic * $pay_days / 31;
                            $calcHRA = $hra * $pay_days / 31;
                            $calcDA = $da * $pay_days / 31;
                            $Total = ($calcBasics + $calcHRA + $calcDA);

                            if (!empty($rse)) {
                                $UserID = $rse[0]->id;
                            } else {
                                $UserID = null;
                            }

                            $salary_process_table_inputs = array(
                                'total' => $Total,
                                'UserID' => $UserID,
                                'pay_days' => $pay_days,
                                'month' => $month,
                                'year' => $year
                            );
                            $pat = Salary_Process::create($salary_process_table_inputs);
                            $monthly_salary_table_inputs = array(
                                'UserID' => $UserID,
                                'basic' => $basic,
                                'HRA' => $hra,
                                'DA' => $da,
                                'month' => $month,
                                'year' => $year
                            );

                            $calMonthly = Monthly_Salary::create($monthly_salary_table_inputs);

                            $file=DB::table('file_upload')
                                ->where('is_file_uploaded', '=',0)
                                ->select('file_upload.*')
                                ->get();

                            if(!empty($file))
                            {
                                $file_path= $file[0]->FilePath;
                            }
                            else{
                                $file_path = null;

                            }
                            $Lab= DB::table('file_upload')
                                ->where('FilePath','=',$file_path)
                                ->update(array(
                                    'is_file_uploaded' => '1',
                                ));

                            var_dump("File Uploaded Successfully");


                        } else {

                            var_dump("Please Provide proper pay days");
                        }


                    } else {

                        var_dump("User Does Not Exists");
                    }

                }

                var_dump("Your Monthly Salary Created Successfully");

            });

        }
    }
}
