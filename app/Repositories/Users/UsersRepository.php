<?php
/**
 * Created by PhpStorm.
 * User: Manish
 * Date: 26-08-2017
 * Time: 13:23
 */

namespace app\Repositories\Users;
use App\Models\Users\User;
use Request;
use Validator;
use Hash;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Doctors\Doctor;
use App\Models\Patients\Patient;
class UsersRepository implements UsersInterface
{

    protected $users;

    public function __construct(User $user, Request $request)
    {
        $this->users = $user;
        $this->request = $request;
    }


    public function get_doctor()
    {
        $patient=Doctor::all();
        if (!empty($patient)) {
            $response['data'] = $patient;
            $response['code'] = 200;
            return $patient;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }
    }

    public function get_patient()
    {
        $doctor=Patient::all();
        if (!empty($doctor)) {
            $response['data'] = $doctor;
            $response['code'] = 200;
            return $doctor;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }

    }





    public function Create_User($input)
    {
        $input= Input::all();
        $rules=array(
            'FirstName'=>'required',
            'LastName'=>'required',
            'EmailId'=>'required|email|unique:users,email',
        );
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
           // $userId = Authorizer::getResourceOwnerId();
            $password = $this->GenerateString();
            DB::beginTransaction();
            $user = User::create([
                'name' => Input::get('FirstName'),
                'email' => Input::get('EmailId'),
                'username' => Input::get('EmailId'),
                'password' => Hash::make($password),
                'FirstName' => Input::get('FirstName'),
                'LastName' => Input::get('LastName'),
                'PhoneNo' => Input::get('PhoneNo'),
                'Gender' => Input::get('Gender'),
                'Address' => Input::get('Address'),
                'City' => Input::get('City'),
                'Country' => Input::get('Country'),
                'Postalcode' => Input::get('Postalcode'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()

            ]);
            $user_input = $user->id;
            $doctor = Doctor::create([
                'UserID' => $user_input,
                'first_name' => Input::get('FirstName'),
                'last_name' => Input::get('LastName'),
                'gender' => Input::get('gender'),
                'date_of_birth' => Input::get('date_of_birth'),
                'specialization' => Input::get('specialization'),
                'address' => Input::get('address'),
                'city' => Input::get('city'),
                'state' => Input::get('state'),
                'pincode' => Input::get('pincode'),
                'country' => Input::get('country'),
                'degree' => Input::get('degree'),
                'EmailId' => Input::get('EmailId'),
                'age' => Input::get('age'),
                'status' => Input::get('status'),
            ]);
            DB::commit();
            $response['data'] = "User Registered Successfully";
            $response['code'] = 200;
            return $response;
        }
        else{
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }

    }

    public function GenerateString()
    {
        $unambiguousCharacters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
        $temporaryPassword = "";
        // build a temporary password consisting of 8 unambiguous characters
        for ($i = 0; $i < 8; $i++) {
            $temporaryPassword .= $unambiguousCharacters[rand(0, strlen($unambiguousCharacters) - 1)];
        }
        return $temporaryPassword;
    }










}