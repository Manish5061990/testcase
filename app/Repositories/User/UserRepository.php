<?php
/**
 * Created by PhpStorm.
 * User: Manish
 * Date: 25-08-2017
 * Time: 15:28
 */

namespace App\Repositories\User;
use App\Models\Users\User;
class UserRepository implements UserRepositoryInterface{

    protected $users;
    public function __construct(User $user,Request $request)
    {
        $this->users = $user;
        $this->request = $request;
    }


    public function find($userId)
    {

        $user=$this->users->findOrFail($userId);
        return $user;

    }




}