<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


$api= app('Dingo\Api\Routing\Router');
$api->version('v1',['prefix' => 'api/v1'], function ($api) {
    // login
    $api->post('login','App\Http\Controllers\OAuth\OAuthController@login');

    //------------------------------Get Excel File Upload---------------------------------------------//

    $api->get('download/employee/sheet','App\Http\Controllers\FileUploadController@GetEmployeeExcelSheetDemo');  // Excel Sheet Download
    $api->get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'App\Http\Controllers\FileUploadController@importExportExcelORCSV')); // form for uploading excel xls,excel xlsx,csv,
    $api->get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'App\Http\Controllers\FileUploadController@downloadExcelFile'));  // Excel file Export


});


$api->version('v1', ['prefix' => 'api/v1','middleware' => 'api.auth','providers' => ['oauth'],'scopes' => ['administrator','doctor']], function ($api) {

    //--------------------------------Post Excel file Upload------------------------------------------------------//

    $api->post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'App\Http\Controllers\FileUploadController@importFileIntoDB'));

});
