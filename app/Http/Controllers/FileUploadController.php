<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Users\Salary_Structure;
use App\Models\Users\Monthly_Salary;
use App\Models\Users\Salary_Process;
use App\Models\Users\FileUploadModel;
use App\Models\Users\User;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Response;
use Illuminate\Support\Facades\Input;

class FileUploadController extends Controller
{

    public function importExportExcelORCSV()
    {
        return view('file_import_export');
    }



    public static function fileattachment($fileattachment_details,$createdby,$is_file_uploaded)
    {
        $filedata = $fileattachment_details;
        $destinationpath = storage_path() . '/uploads/';
       // dd($destinationpath);
        if (!is_dir($destinationpath)) {

            mkdir(storage_path() . '/uploads/', 0777);
        }

        if (isset($filedata))
        {
            $filetype['originalname'] = $filedata->getClientOriginalName();

            $filetype['realpath'] = $filedata->getRealPath();

            $filetype['originalextension'] = $filedata->getClientOriginalExtension();

            $filetype['mimetype'] = $filedata->getMimeType();

            $filetype['filesize'] = $filedata->getSize();

            $filetype['name'] = trim($filetype['originalname']);

            $filepath = $filedata->move($destinationpath, time().$filetype['name']);

            $filetype['filepath'] = $filepath->getRealPath();

            $filepath = asset('uploads/'.time().$filetype['name']);

        }
        else{

            $filetype = "null";
            $filepath = "null";
        }

        $fileattachment = FileUploadModel::create([
            'FilePath' => $filepath,
            'FileType' =>$filetype['originalextension'],
            'CreatedBy'=> $createdby,
            'is_file_uploaded'=> $is_file_uploaded
        ]);
    }


    public function importFileIntoDB(Request $request)
    {
        ini_set('memory_limit', -1);
        $filedata = Input::file('LoadEmpData');

        $is_file_uploaded = 0;
        $createdby = 1;
        $this->fileattachment($filedata,$createdby,$is_file_uploaded);





    }

    public function GetEmployeeExcelSheetDemo()
    {
        $sheet = file_get_contents(base_path() . '/database/seeds/Files/LoadEmpData.csv');
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="LoadEmpData.csv"',
        );
        return Response::make(rtrim($sheet, "\n"), 200, $headers);
    }


    public function downloadExcelFile($type){
        $products = Salary_Process::get()->toArray();

        return Excel::create('LoadEmpData', function($excel) use ($products) {
            $excel->sheet('sheet name', function($sheet) use ($products)
            {
                $sheet->fromArray($products);
            });
        })->download($type);
    }
}





